#pragma once
#include <vector>
#include <cassert>
#include <QPoint>
#include <QSize>


enum class CellType { Pass, Obstacle };


struct Field {
	struct Node : public QPoint {
	};

	struct NodeHash {
		std::size_t operator()(Node const& s) const noexcept {
			std::size_t h1 = std::hash<int>{}(s.x());
			std::size_t h2 = std::hash<int>{}(s.y());
			return h1 ^ (h2 << 1); // or use boost::hash_combine
		}
	};

protected:
	auto indexFrom2d(const QPoint& position) const {
		return static_cast<size_t>(position.x() + position.y() * _size.width());
	}

public:
	Field(QSize size)
		: _size {size}
		, cells{ static_cast<size_t>(_size.width() * _size.height()), CellType::Pass }
	{
	}

	CellType getCellType(const QPoint& position) const {
		return cells.at(indexFrom2d(position));
	}

	void setCellType(const QPoint& position, const CellType& cellType) {
		cells.at(indexFrom2d(position)) = cellType;
	}

	bool isPassable(const QPoint& position) const {
		return cells.at(indexFrom2d(position)) == CellType::Pass;
	}

	QSize getSize() const {
		return _size;
	}


protected:
	QSize _size {};
	std::vector<CellType> cells;
};

