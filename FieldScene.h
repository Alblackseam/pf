#pragma once
#include <set>
#include <memory>
#include <optional>
#include <QGraphicsScene>
#include <QThread>
#include "Field.h"
#include "PathFindWorker.h"


class FieldScene : public QGraphicsScene {
	Q_OBJECT

public:
	FieldScene(QObject *parent = nullptr)
		: QGraphicsScene{parent}
	{
	}

	~FieldScene();

	void onMouseMove(QPointF pos);
	void onMouseLeftButton(QPointF pos);
	void onMouseMiddleButton(QPointF pos);
	void onMouseRightButton(QPointF pos);
	void generateRandomField(int width, int height);
    void makeFieldItems();
    void makeStartItem();

protected:
	std::shared_ptr<Field> field;
	QGraphicsTextItem* textStartItem {};

	std::unordered_map<QGraphicsItem*, Field::Node> itemToNode;
	std::optional<Field::Node> getNodeByMousePos(QPointF pos);

	QPoint _startPosition;
	QPoint _finishPosition;
	bool setFinishPosition(QPoint position);

	std::set<QGraphicsItem*> pathItems;
	void visualizePath(const std::list<Field::Node>& path);
	//void visualizeGraph(const std::unordered_map<Field::Node, Field::Node, Field::NodeHash>& graph);

	void startPathFindThread();
	void stopPathFindThread();
	PathFindWorker* pathFindWorker {};
	QThread* pathFindThread {};

	static constexpr auto cellSizePixels = 40.0;

public slots:
    void pathFindThreadDone();
};

