#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <stdexcept>
#include <QIntValidator>
#include <QSettings>
#include "FieldView.h"


MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	ui->newFieldWidth->setValidator( new QIntValidator(1, 500, this) );
	ui->newFieldHeight->setValidator( new QIntValidator(1, 500, this) );

	fieldScene = new FieldScene(this);
	graphicsView = new FieldView(ui->graphicsViewFrame);
	ui->graphicsViewFrame->layout()->addWidget(graphicsView);
	graphicsView->setScene(fieldScene);
}


MainWindow::~MainWindow() {
	saveWindowPosition();
	delete ui;
}


void MainWindow::restoreWindowPosition() {
	QSettings settings;
	//settings.clear();

	try {
		auto getIntSettingValue = [&](std::string valueName) {
			auto valueFullName = objectName() + "/" + valueName.c_str();
			auto var = settings.value(valueFullName);
			if (!var.isValid())
				throw std::runtime_error("invalid value " + valueFullName.toStdString());
			if (!var.canConvert<int>())
				throw std::runtime_error("invalid value " + valueFullName.toStdString());
			return var.template value<int>();
		};

		auto x = getIntSettingValue("x");
		auto y = getIntSettingValue("y");
		auto w = getIntSettingValue("width");
		auto h = getIntSettingValue("height");

		move(x, y);
		resize(w, h);
	} catch (...) {
	}
}

void MainWindow::saveWindowPosition() {
	QSettings settings;

	std::map<std::string, int> conf;
	conf["x"] = x();
	conf["y"] = y();
	conf["width"] = width();
	conf["height"] = height();
	for (const auto& confItem : conf) {
		settings.setValue(objectName() + "/" + confItem.first.c_str(), confItem.second);
	}
}


void MainWindow::on_generateNew_clicked() {
	auto lineEditParseInt = [](QLineEdit* le) -> std::optional<int> {
		QString text = le->text();
		int val{};
		auto validator = le->validator();
		if (validator) {
			auto state = validator->validate(text, val);
			if (state != QValidator::State::Acceptable) {
				throw std::runtime_error("invalid input `" + le->text().toStdString() + "` in field `" + le->objectName().toStdString() + "`");
			}
		}

		bool ok = false;
		val = le->text().toInt(&ok);
		if (!ok)
			throw std::runtime_error("invalid input `" + le->text().toStdString() + "` in field `" + le->objectName().toStdString() + "`");
		return val;
	};

	auto fieldWidth = lineEditParseInt(ui->newFieldWidth);
	auto fieldHeight = lineEditParseInt(ui->newFieldHeight);

	fieldScene->generateRandomField(*fieldWidth, *fieldHeight);
    fieldScene->makeFieldItems();
    fieldScene->makeStartItem();
}
