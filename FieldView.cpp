#include "FieldView.h"
#include <QWheelEvent>
#include <QtMath>


void FieldView::wheelEvent(QWheelEvent *wheelEvent) {
	double angle = wheelEvent->angleDelta().y();
	double factor = qPow(1.0015, angle);

	auto targetViewportPos = wheelEvent->position();
	auto targetScenePos = mapToScene(targetViewportPos.x(), targetViewportPos.y());

	scale(factor, factor);
	centerOn(targetScenePos);
	auto deltaViewportPos = targetViewportPos - QPointF(viewport()->width() / 2.0, viewport()->height() / 2.0);
	auto viewportCenter = mapFromScene(targetScenePos) - deltaViewportPos;
	centerOn(mapToScene(viewportCenter.toPoint()));
}

void FieldView::mouseMoveEvent(QMouseEvent *event) {
	if (startDragPosition) {
		auto currentPosition = mapToScene(event->pos());
		auto shift = currentPosition - *startDragPosition;

		auto viewportCenter = mapToScene(QPoint(viewport()->width() / 2.0, viewport()->height() / 2.0));
		centerOn(viewportCenter - shift);
		return;
	}

	if (_fieldScene)
		_fieldScene->onMouseMove(mapToScene(event->pos()));
}

void FieldView::mousePressEvent(QMouseEvent *event) {
	auto positionSceneSpace = mapToScene(event->pos());

	if (event->buttons() & Qt::MiddleButton) {
		startDragPosition = positionSceneSpace;
	}
}

void FieldView::mouseReleaseEvent(QMouseEvent* event) {
	startDragPosition.reset();
	auto positionSceneSpace = mapToScene(event->pos());

	if ((event->button() & Qt::LeftButton) > 0) {
		if (_fieldScene)
			_fieldScene->onMouseLeftButton(positionSceneSpace);
	}

	if ((event->button() & Qt::MiddleButton) > 0) {
		if (_fieldScene)
			_fieldScene->onMouseMiddleButton(positionSceneSpace);
	}

	if ((event->button() & Qt::RightButton) > 0) {
		if (_fieldScene)
			_fieldScene->onMouseRightButton(positionSceneSpace);
	}
}







