#include "PathFindWorker.h"

void PathFindWorker::process()
{
	graph = _pathFinder.find(_startPosition, _finishPosition);
	path = _pathFinder.getPath(graph, _startPosition, _finishPosition);

	if (!_pathFinder.isCanceled())
		emit pathFound();

	emit finished();
}
