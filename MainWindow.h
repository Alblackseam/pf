#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "FieldView.h"
#include "FieldScene.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();

	void restoreWindowPosition();
	void saveWindowPosition();

public slots:
	void on_generateNew_clicked();

private:
	Ui::MainWindow *ui;

	FieldView* graphicsView;
	FieldScene* fieldScene;

};
#endif // MAINWINDOW_H
