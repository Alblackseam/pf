#pragma once
#include <QGraphicsView>
#include <FieldScene.h>


class FieldView : public QGraphicsView
{
	Q_OBJECT
public:
	FieldView(QWidget* parent)
		: QGraphicsView(parent)
	{
		setMouseTracking(true);
	}

	void wheelEvent(QWheelEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void mousePressEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;

	void setScene(FieldScene* fieldScene) {
		_fieldScene = fieldScene;
		QGraphicsView::setScene(_fieldScene);
	}

protected:
	FieldScene* _fieldScene;
	std::optional<QPointF> startDragPosition;
};


