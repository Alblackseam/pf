#pragma once
#include <QObject>
#include <QPoint>
#include "Field.h"
#include "PathFinder.h"


class PathFindWorker : public QObject {
	Q_OBJECT

public:
	PathFindWorker(const Field& field, QPoint startPosition, QPoint finishPosition)
		: QObject(nullptr)
		, _field{field}
		, _pathFinder{_field}
		, _startPosition{startPosition}
		, _finishPosition{finishPosition}
	{
	}

	PathFinder::Graph graph;
	std::list<Field::Node> path;

public slots:
	void process();

	void setCancelFlag() {
		_pathFinder.setCanceled();
	}

signals:
	void pathFound();
	void finished();

private:
	Field _field;
	PathFinder _pathFinder;
	QPoint _startPosition;
	QPoint _finishPosition;
};

