#pragma once
#include <queue>
#include <list>
#include <array>
#include <unordered_map>
#include "Field.h"


struct PathFinder {
	using Node = Field::Node;
	using Graph = std::unordered_map<Node, Node, Field::NodeHash>;

	PathFinder(Field& field)
		: _field{field}
	{
	}

	std::list<Node> getPath(const Graph& visitedFrom, const QPoint& startPosition, const QPoint& finishPosition) {
		std::list<Node> path;

		auto tail = Node{finishPosition};
		for (auto it = visitedFrom.find(tail); it != visitedFrom.end(); it = visitedFrom.find(tail)) {
			path.push_front(it->first);
			if (it->first == startPosition)
				return path;
			tail = it->second;
		}
		return path;
	}

	Graph find(const QPoint& startPosition, const QPoint& finishPosition) const {
		auto goal = Node{finishPosition};

		std::queue<Node> frontier;
		frontier.push({startPosition});
		Graph visitedFrom;
		visitedFrom[{startPosition}] = {startPosition}; // null ?

		while (!frontier.empty()) {
			if (_canceled) return {};

			auto current = frontier.front();
			frontier.pop();

			if (current == goal)
			   return visitedFrom;

			auto neighbors = getPassableNeighbors(current);
			for (const auto& next : neighbors) {
				auto it = visitedFrom.find(next);
				if (it == visitedFrom.end()) {
					frontier.push(next);
					visitedFrom[next] = current;
				}
			}
		}
		return {};
	}

	void setCanceled() {
		_canceled = true;
	}

	bool isCanceled() const {return _canceled; }

protected:
	Field& _field;
	std::atomic<bool> _canceled {false};

	bool isInRange(const QPoint& position) const {
		return (position.x() >= 0)
			&& (position.x() < _field.getSize().width())
			&& (position.y() >= 0)
			&& (position.y() < _field.getSize().height());
	}

	std::list<Node> getPassableNeighbors(const Node& current) const {
		static std::array<QPoint, 4> directions = {
			QPoint( 1,  0),
			QPoint( 0,  1),
			QPoint(-1,  0),
			QPoint( 0, -1)
		};

		std::list<Node> result;
		for (auto dir : directions) {
			if (isInRange(current + dir)) {
				if (_field.isPassable(current + dir)) {
					result.push_back(Node{current + dir});
				}
			}
		}
		return result;
	}
};
