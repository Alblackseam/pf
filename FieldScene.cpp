#include "FieldScene.h"
#include "PathFinder.h"
#include <QGraphicsTextItem>


void FieldScene::onMouseLeftButton(QPointF pos) {
	auto node = getNodeByMousePos(pos);
	if (node) {
		if (field->isPassable(*node)) {
			_startPosition = *node;
            makeStartItem();
		}
	}
}


void FieldScene::onMouseMiddleButton(QPointF) {
}


void FieldScene::onMouseRightButton(QPointF pos) {
	auto node = getNodeByMousePos(pos);
	if (node && field) {
		auto finishPosition = *node;
		if (!setFinishPosition(finishPosition)) return;

		stopPathFindThread();
		startPathFindThread();
	}
}


FieldScene::~FieldScene()
{
	stopPathFindThread();
}

void FieldScene::onMouseMove(QPointF pos) {
	auto node = getNodeByMousePos(pos);
	if (node && field) {
		auto finishPosition = *node;
		if (!setFinishPosition(finishPosition)) return;

		stopPathFindThread();
		startPathFindThread();
	}
}

void FieldScene::generateRandomField(int width, int height) {
	stopPathFindThread();
	static constexpr auto obstaclePossibilityProcents = 25;

	field = std::make_shared<Field>(QSize{width, height});

	for (auto y = 0; y < height; ++y) {
		for (auto x = 0; x < width; ++x) {
			auto cellType = rand() % 100 < obstaclePossibilityProcents ? CellType::Obstacle : CellType::Pass;
			field->setCellType({x, y}, cellType);
		}
	}

	auto genRandomPos = [&]() { return QPoint{ rand() % width, rand() % height }; };

	auto randPos = genRandomPos();
	for (; !field->isPassable(randPos); randPos = genRandomPos()) {
	}
	_startPosition = randPos;

	randPos = genRandomPos();
	for (; !field->isPassable(randPos); randPos = genRandomPos()) {
	}
	_finishPosition = randPos;

}


void FieldScene::makeFieldItems() {
	static constexpr auto margin = 2.0;

	auto width = field->getSize().width();
	auto height = field->getSize().height();

    textStartItem = nullptr;
	pathItems.clear();
	itemToNode.clear();
	clear();

	auto rectMargin = 0.1;
	addRect(-rectMargin * cellSizePixels, -rectMargin * cellSizePixels, (width + rectMargin * 2) * cellSizePixels, (height + rectMargin * 2) * cellSizePixels, QPen{QColor{{u"orange"}}} );

	for (auto y = 0; y < height; ++y) {
		for (auto x = 0; x < width; ++x) {
			auto cellType = field->getCellType({x, y});

			auto color = cellType == CellType::Pass ? QColor{u"honeydew"} : QColor{u"lightcoral"};
			auto rectItem = addRect(QRectF{x * cellSizePixels + margin, y * cellSizePixels + margin, cellSizePixels - margin * 2, cellSizePixels - margin * 2}, QPen{color}, QBrush{color} );

			itemToNode[rectItem] = Field::Node{{x, y}};
		}
	}
}


void FieldScene::makeStartItem() {
    if (textStartItem)
        removeItem(textStartItem);

    textStartItem = addText("S", QFont("Arial", cellSizePixels * 0.5));
    textStartItem->setPos(_startPosition * cellSizePixels);
}

std::optional<Field::Node> FieldScene::getNodeByMousePos(QPointF pos) {
	auto itemsAtPosition = items(pos);

	for (auto item : itemsAtPosition) {
		auto it = itemToNode.find(item);
		if (it != itemToNode.end()) {
			return it->second;
		}
	}
	return {};
}


bool FieldScene::setFinishPosition(QPoint position) {
	if (position == _startPosition) return false;
	if (position == _finishPosition) return false;
	_finishPosition = position;
	return true;
}


void FieldScene::visualizePath(const std::list<Field::Node>& path) {
	for (auto* item : pathItems)
		removeItem(item);
	pathItems.clear();


	if (path.empty())
		return;

	QPainterPath painterPath;
	std::optional<Field::Node> prevNode;
	for (const auto& node : path) {
		static constexpr auto halfCell = QPointF{0.5, 0.5};
		auto nodePosPixels = (node + halfCell) * cellSizePixels;

		if (prevNode) {
			painterPath.lineTo(nodePosPixels);
		} else {
			painterPath.moveTo(nodePosPixels);
		}
		prevNode = node;
	}

	pathItems.insert(addPath(painterPath));
}


//void FieldScene::visualizeGraph(const PathFinder::Graph& graph) {
//	for (auto* item : pathItems)
//		removeItem(item);
//	pathItems.clear();
//	for (auto [node, nodeFrom] : graph) {
//		auto nodePosPixels = (node + QPointF{0.5, 0.5}) * cellSizePixels;
//		auto nodeDirPixels = (node - QPointF(nodeFrom)) * cellSizePixels;
//		auto item = addLine(QLineF{nodePosPixels - nodeDirPixels * 0.4, nodePosPixels - nodeDirPixels * 0.1}, {});
//		pathItems.insert(item);
//	}
//}


void FieldScene::startPathFindThread() {
	if (!field) return;

	assert(pathFindWorker == nullptr);
	pathFindWorker = new PathFindWorker{*field, _startPosition, _finishPosition};
	pathFindThread = new QThread;

	pathFindWorker->moveToThread(pathFindThread);
	connect(pathFindThread, SIGNAL(started()), pathFindWorker, SLOT(process()));
    connect(pathFindWorker, SIGNAL(pathFound()), this, SLOT(pathFindThreadDone()), Qt::QueuedConnection);
	connect(pathFindWorker, SIGNAL(finished()), pathFindThread, SLOT(quit()));
	pathFindThread->start();
}

void FieldScene::stopPathFindThread()
{
	if (pathFindWorker) {
		assert(pathFindThread);
		pathFindWorker->setCancelFlag();

		pathFindThread->quit();
		pathFindThread->wait();
		delete pathFindThread;
		pathFindThread = nullptr;

		delete pathFindWorker;
		pathFindWorker = nullptr;
	}
}


void FieldScene::pathFindThreadDone()
{
	if (pathFindWorker) {
		visualizePath(pathFindWorker->path);
		stopPathFindThread();
	}
}

