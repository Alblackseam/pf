#include "MainWindow.h"
#include <QApplication>
#include <QMessageBox>
#include <QDebug>
// git clone https://Alblackseam@bitbucket.org/Alblackseam/pf.git


struct Application : public QApplication {
	Application(int &argc, char **argv) : QApplication(argc, argv) {
	}

	bool notify(QObject* receiver, QEvent* event) override {
		try {
			return QApplication::notify(receiver, event);
		} catch (const std::exception& e) {
			qWarning("Error \"%s\" sending event \"%s\" to object %s (%s)", e.what(), typeid(*event).name(), qPrintable(receiver->objectName()), typeid(*receiver).name());
			QMessageBox::information(messageBoxParent, "Error", e.what());
		} catch (...) {
			qWarning("Error \"Unknown\" sending event \"%s\" to object %s (%s)", typeid(*event).name(), qPrintable(receiver->objectName()), typeid(*receiver).name());
			QMessageBox::information(messageBoxParent, "Error", "Unknown error");
		}
		return false;
	}

	QWidget* messageBoxParent = nullptr;
};


int main(int argc, char *argv[]) {
	Application a(argc, argv);
	QCoreApplication::setOrganizationName("MyOrganization");
	QCoreApplication::setOrganizationDomain("mysoft.com");
	QCoreApplication::setApplicationName("Path Finder");

	MainWindow w;
	a.messageBoxParent = &w;
	w.restoreWindowPosition();
	w.on_generateNew_clicked();
	w.show();
	return a.exec();
}
